### Аргументы командной строки ###
(Аргументы командной строки)[https://bitbucket.org/koi08r/scrap_coinmarketcap/wiki/Command%20line%20switches%20%5BRU%5D]

### Установить/Обновить ###
```bash
python -m pip install --upgrade https://bitbucket.org/koi08r/scrap_coinmarketcap/get/release.zip
```
### Скачать и вывести на экран данные BTC, LTC с 1 декабря 2017г. с CSV заголовками ###
```bash
python2 -m scrap_coinmarketcap -f - init -s 01122017 -t BTC LTC -q 16
```
