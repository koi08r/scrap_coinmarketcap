# coding=utf-8
from __future__ import print_function, unicode_literals
import logging
from time import sleep


logging.basicConfig(
    level=logging.DEBUG,
    format="[%(levelname)s] (%(asctime)s) %(name)s:%(lineno)d - %(message)s"
)
log = logging.getLogger('gevent-http-test')


def application(env, start_response):
    path = env.get('PATH_INFO', '/').lstrip('/').encode('utf-8')
    log.debug(path)
    sleep(int(path))
    start_response('200 OK', [('Content-Type', 'text/plain; charset=utf-8'),
                              ('Content-Length', '{}'.format(len(path)))])
    return iter(path)
