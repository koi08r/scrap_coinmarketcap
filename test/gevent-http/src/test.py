import gevent.monkey
from pprint import pprint as pp
from requests import get as http_get


gevent.monkey.patch_all()


def fn_thread(r):
    print('Got #{}'.format(r))
    return http_get('http://127.0.0.1:8491/{}'.format(r)).content.lstrip('/')


thread_pool = []

thread_pool.append(gevent.spawn(fn_thread, 5))
thread_pool.append(gevent.spawn(fn_thread, 0))
thread_pool.append(gevent.spawn(fn_thread, 3))
thread_pool.append(gevent.spawn(fn_thread, 1))


pp([_.value for _ in gevent.joinall(thread_pool)])
