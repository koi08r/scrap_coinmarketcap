#!/bin/bash

python2.7 -m gunicorn.app.wsgiapp \
  --access-logfile - --workers 3 \
  --access-logformat "%(h)s %(l)s %(r)s %(l)s %(s)s"\
  --log-level info \
  --reload \
  --chdir src/ \
  --bind :8491 \
  app:application
