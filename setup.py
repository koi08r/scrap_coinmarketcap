from setuptools import setup


setup(
    name='scrap_coinmarketcap',
    version='0.0.2',
    url='https://bitbucket.org/koi08r/scrap_coinmarketcap',
    description=("Parse html and save to csv from urls:\n"
                 "  - https://coinmarketcap.com/all/views/all/\n"
                 "  - https://coinmarketcap.com/currencies/{symbol}/historical-data/?start={start}&end={end}"),
    package_dir={'': 'src'},
    packages=['scrap_coinmarketcap'],
    include_package_data=True,
    install_requires=[
        'lxml',
        'requests',
        'gevent',
        'six',
    ],
    author='koi8-r',
    author_email='koi8-r@oz.net.ru',
    license='MIT',
    zip_safe=False,
    scripts=[],
    classifiers=[
        'Environment :: Console',
        'Programming Language :: Python :: 2.7'
    ]
)
