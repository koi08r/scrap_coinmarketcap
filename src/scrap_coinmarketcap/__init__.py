from __future__ import unicode_literals, print_function
from six import text_type
import six
import sys
import logging
from os import SEEK_END, linesep
import lxml.html
import csv
from requests import get as http_get, Session as HTTPSession
from requests.adapters import HTTPAdapter
from requests.compat import urljoin
from gevent import joinall
import gevent.monkey
from locale import setlocale, LC_TIME, LC_NUMERIC
from datetime import datetime as dt
from argparse import ArgumentParser, ArgumentTypeError
from pprint import pprint as pp


__all__ = ['main']
__version__ = '0.0.2'

# no warn
assert pp == pp
assert http_get == http_get
assert linesep == linesep


class Float(float):
    """Write 0.000070, not 7e-05 during CSV float serialization."""
    fmt = '{:f}'.format

    def __repr__(self):
        return self.__class__.fmt(self)


def main():

    # locale for date parse (month short form)
    setlocale(LC_TIME, 'C')
    setlocale(LC_NUMERIC, 'C')

    # patch_socket, patch_ssl, patch_dns, ...
    gevent.monkey.patch_all()

    logging.basicConfig(
        level=logging.ERROR,
        format="[%(levelname)s] (%(asctime)s) %(name)s:%(lineno)d - %(message)s"
    )
    log = logging.getLogger('com.coinmarketcap.app')
    logging.getLogger('urllib3').level = logging.ERROR

    # !!! ARGV parse !!!

    common_argparser = ArgumentParser(
        description="%(prog)s"
                    'Parse html and save to csv from urls:'
                    '  - https://coinmarketcap.com/all/views/all/'
                    '  - https://coinmarketcap.com/currencies/{symbol}/historical-data/?start={start}&end={end}',
                    # https://api.coinmarketcap.com/v1/ticker/?start=0&limit=
        epilog='',
        add_help=False
    )

    def arg_parse_date(d):
        try:
            return dt.strptime(d, '%d%m%Y').date()
        except ValueError:
            raise ArgumentTypeError('Not a valid date {}'.format(d))

    common_argparser.add_argument('--start', '-s', type=arg_parse_date, help='start date',
                                  default=dt(1990, 1, 1).date())
    common_argparser.add_argument('--end', '-e', type=arg_parse_date, help='end date', default=dt.now().date())
    # common_argparser.add_argument('symbols', nargs='*')
    common_argparser.add_argument('--symbols', '-t', nargs='*', action='append', default=[],
                                  type=lambda _: _.upper().encode('utf-8'))
    common_argparser.add_argument('-q', action='store', type=int, default=99, help='number of parallel requests')

    main_argparser = ArgumentParser(
        prog=__name__,
        description="Main %(prog)s"
    )
    main_argparser.add_argument('--verbose', '-v', action='store_const', const=True, default=False,
                                help='Verbose output')
    main_argparser.add_argument('--version', '-V', action='version', version='Version: %(prog)s {}'.format(__version__))
    main_argparser.add_argument('--debug', '-d', action='count', default=0)
    main_argparser.add_argument('--file', '-f', type=text_type, required=True)

    cmd_argparser = main_argparser.add_subparsers(help='', description='', dest='command')

    cmd_argparser.add_parser('init', help='init database', parents=[common_argparser])

    updatecmd_argparser = cmd_argparser.add_parser('append', help='append database', parents=[common_argparser])
    updatecmd_argparser.set_defaults(inteligence=False)

    # lastcmd_argparser = cmd_argparser.add_parser('last', help='last record date')
    # lastcmd_argparser.add_argument('--reverse', '-r', action='store_true', default=False, help='Read from EOF')

    opts = main_argparser.parse_args(
        # ['-f', 'file.csv', '-dd',  'init',  '-q',  '1',  '-s',  '01012014',  '-e',  '01042014',  '-t',  '42',  'LTC',]
    ).__dict__
    # pp(opts)

    start = opts['start'].strftime('%Y%m%d')
    end = opts['end'].strftime('%Y%m%d')
    base_uri = 'https://coinmarketcap.com/'
    file_name = opts['file']
    n_threads = opts['q']
    symbols = [__ for _symel in opts['symbols'] for __ in _symel if __]

    if opts['debug']:
        log.level = logging.INFO
    if opts['verbose'] or opts['debug'] > 1:
        log.level = logging.DEBUG
        if opts['debug'] > 2:
            logging.getLogger('urllib3').level = logging.INFO
        if opts['debug'] > 3:
            logging.getLogger('urllib3').level = logging.DEBUG

    def readline_last(fname):
        # type: (six.binary_type) -> six.text_type
        with open(fname, b'rb') as fh:
            fh.seek(0, SEEK_END)
            size = pos = f.tell()
            res_ln = b''
            while pos > 0:
                s = fh.read(1)
                if s == '\n' and pos + 1 != size:
                    break
                else:
                    res_ln += s
                    pos -= 1
                    fh.seek(pos)

            return res_ln.decode('utf-8')[::-1].strip()

    assert readline_last == readline_last  # no warn

    # !!! Entry point !!!
    with(open(file_name.encode('utf-8'), b'a' if opts['command'] == 'append' else b'w' + b'b')
         if file_name != '-'
         else sys.stdout) as f:

        w = csv.writer(f,
                       delimiter=b';',
                       quotechar=b'"',
                       quoting=csv.QUOTE_NONNUMERIC,
                       lineterminator="\n")  # os.linesep bug: \r\r\n

        if opts['command'] == 'init':
            w.writerow([b'Symbol', b'Date', b'Open', b'High', b'Low', b'Close', b'Volume', b'Market Cap'])

        __COUNTER__ = 0

        def data_handler(row):
            # (r.keys()[0],) + rr for r in row for rr in r.values()[0]
            for r in row:
                sym = r.keys()[0]
                for rr in r[sym]:
                    # pp((sym,) + rr)
                    result = \
                        (sym, dt.strptime(rr[0], '%b %d, %Y').strftime('%d%m%Y'),) + \
                        tuple((Float(_.replace(',', '')) if _ else None for _ in rr[1:5])) + \
                        tuple((int(round(Float(_.replace(',', '')) / 1000)) if _ else None for _ in rr[5:]))
                    w.writerow(result)

        session = HTTPSession()
        session.mount('https://', HTTPAdapter(max_retries=99,
                                              pool_connections=n_threads,
                                              pool_maxsize=n_threads))
        thread_pool = []
        for _symel in lxml.html \
                          .document_fromstring(session.get(urljoin(base_uri, '/all/views/all/')).content) \
                          .xpath('//table[@id="currencies-all"]/tbody/tr/td/span[@class="currency-symbol"]/a'):

            __COUNTER__ += 1

            cur_sym = _symel.text_content().strip()
            hist_uri = urljoin(base_uri,
                               urljoin(_symel.attrib['href'],
                                       'historical-data/?start={start}&end={end}'.format(
                                       start=start,
                                       end=end)))

            if symbols and cur_sym not in symbols:
                log.debug('Skip {}'.format(cur_sym))
                continue

            def fn_thread(tid, sym, uri):
                log.info('Thread {} fetch history for {} from {}'.format(tid, sym, uri))
                rows = {sym: []}  # '_meta': (sym, uri)

                content = session.get(uri).content
                # content = http_get(uri).content
                for _histel in lxml.html \
                                   .document_fromstring(content) \
                                   .xpath('//div[@id="historical-data"]'
                                          '//div[@class="table-responsive"]/table/tbody/tr'):

                    vals = _histel.xpath('./td/text()')
                    if len(vals) != 7:  # len(td) == 1 and td == 'No data was found for the selected time period.':
                        return rows

                    t = tuple(v.lstrip('-').strip() or None for v in _histel.xpath('./td/text()'))
                    log.debug('Fetched for {} data {}'.format(sym, t))
                    rows[sym].append(t)
                return rows

            if len(thread_pool) >= n_threads:
                log.debug('Joining threads: {}'.format(len(thread_pool)))
                # results = [_.value for _ in joinall(thread_pool)]
                results = []
                for _ in joinall(thread_pool):
                    # todo: get _.exception from greenlet
                    # todo: iter thread_pool and wait
                    if not _.value:
                        raise RuntimeError('Error during server query')
                    else:
                        results.append(_.value)
                data_handler(results)
                thread_pool = []

            log.debug('Append thread: {} for {} from {}'.format(len(thread_pool) + 1, cur_sym, hist_uri))
            log.info('Task {} for {}'.format(__COUNTER__, cur_sym))
            thread_pool.append(gevent.spawn(fn_thread,
                                            len(thread_pool) + 1,
                                            cur_sym,
                                            hist_uri))

        if len(thread_pool):
            log.debug('Joining threads tail: {}'.format(len(thread_pool)))
            # data_handler([_.value for _ in joinall(thread_pool)])  # timeout=1
            results = []
            for _ in joinall(thread_pool):
                if not _.value:
                    raise RuntimeError('Error during server query')
                else:
                    results.append(_.value)
            data_handler(results)
