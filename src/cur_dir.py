import sys
from os import path


if getattr(sys, 'frozen', False):
    SCRIPT_FOLDER = path.dirname(sys.executable)
else:
    SCRIPT_FOLDER = path.dirname(path.realpath(__file__))
